import validator from "validator";

class ValidateFields {
  validateText(text) {
    if (validator.isEmpty(text)) {
      return false;
    }
    return true;
  }
}

const validateFields = new ValidateFields();
export { validateFields };
