import React, { useState } from 'react';
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import { Link } from "react-router-dom"
import axios from 'axios'
import { validateFields } from './Validation'

function Update(props) {
  const dataView = props.dataOfArticle.find((item) => item.ID == props.match.params.ID);
  const [title, setTitle] = useState(dataView.TITLE);
  const [description, setDescription] = useState(dataView.DESCRIPTION);
  const [labelTitleMsg, setLabelTitleMsg] = useState({
    display: "none",
    color: "red",
  })
  const [labelDescMsg, setLabelDescMsg] = useState({
    display: "none",
    color: "red",
  })
  const [image, setImage] = useState("https://cdn.shortpixel.ai/spai/w_924+q_lossy+ret_img+to_webp/https://centralcoastdentalimplants.com/wp-content/uploads/2016/10/orionthemes-placeholder-image-750x750.jpg");

  const handleSubmit = evt => {
    evt.preventDefault();

    const isTitleNotEmpty = validateFields.validateText(title);
    const isDescNotEmpty = validateFields.validateText(description);

    if (isTitleNotEmpty == false) {
      setLabelTitleMsg({
        display: "block",
        color: "red"
      })
    } else {
      setLabelTitleMsg({
        display: "none",
        color: "red"
      })
    }

    if (isDescNotEmpty == false) {
      setLabelDescMsg({
        display: "block",
        color: "red"
      })
    } else {
      setLabelDescMsg({
        display: "none",
        color: "red"
      })
    }

    if (isTitleNotEmpty == true && isDescNotEmpty == true) {
      let article = {
        TITLE: title,
        DESCRIPTION: description,
        IMAGE: image
      }

      axios.put(`http://110.74.194.124:15011/v1/api/articles/${dataView.ID}`, article).then((res) => {
        alert(res.data.MESSAGE)
        props.history.push("/")
      })
      setLabelTitleMsg({
        display: "none",
        color: "red"
      })
      setLabelDescMsg({
        display: "none",
        color: "red"
      })
    }
  }

  const handleOnImageChange = evt => {
    if (evt.target.files && evt.target.files[0]) {
      setImage(URL.createObjectURL(evt.target.files[0]))
    }
  }

  return (
    <div>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <div className="container">
          <Navbar.Brand>AMS</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link as={Link} to="/">Home</Nav.Link>
            </Nav>
            <Nav>
              <Form inline>
                <FormControl
                  type="text"
                  placeholder="Search"
                  className="mr-sm-2"
                />
                <Button variant="outline-success">Search</Button>
              </Form>
            </Nav>
          </Navbar.Collapse>
        </div>
      </Navbar>
      <div className="container">
        <h1>Update Article</h1>
        <div className="row">
          <div className="col-9">
            <Form onSubmit={handleSubmit} autoComplete="off">
              <Form.Group>
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Title"
                  name="title"
                  value={title}
                  onChange={event => setTitle(event.target.value)}
                />
                <p style={labelTitleMsg}>*Title Required</p>
              </Form.Group>
              <Form.Group>
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Description"
                  name="description"
                  value={description}
                  onChange={event => setDescription(event.target.value)}
                />
                <p style={labelDescMsg}>*Descriptoin Required</p>
              </Form.Group>
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>
          </div>
          <div className="col-3">
            <img className="img-fluid" style={{ width: "100%", marginBottom: "5px" }} src={image} alt="photo" />
            <Form.File.Input onChange={handleOnImageChange} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Update
