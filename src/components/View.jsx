import React from 'react'
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import { Link } from "react-router-dom"

function View(props) {
    var dataView = props.dataOfArticle.find((item) => item.ID == props.match.params.ID);
    return (
        <div>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <div className="container">
                    <Navbar.Brand>AMS</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link as={Link} to="/">Home</Nav.Link>
                        </Nav>
                        <Nav>
                            <Form inline>
                                <FormControl
                                    type="text"
                                    placeholder="Search"
                                    className="mr-sm-2"
                                />
                                <Button variant="outline-success">Search</Button>
                            </Form>
                        </Nav>
                    </Navbar.Collapse>
                </div>
            </Navbar>
            <div className="container">
                <h2 style={{ margin: "20px 0px" }}>Article</h2>
                <div className="row">
                    <div className="col-3">
                        <img src={dataView.IMAGE} alt="Avatar" style={{ width: "100%" }} />
                    </div>
                    <div className="col-9">
                        <h3>Title : {dataView.TITLE}</h3>
                        <p>{dataView.DESCRIPTION}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default View