import React, { Component } from 'react'
import { Navbar, Nav, Form, FormControl, Table, Button } from "react-bootstrap";
import { Link } from "react-router-dom"
import axios from "axios";

export default class MainPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            article: [],
            searchValue: '',
        };
    }

    componentWillMount() {
        axios
            .get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
            .then((res) => {
                this.setState({
                    article: res.data.DATA,
                });
            });
    }

    handleChange = (evt) => {
        this.setState({
            searchValue: evt.target.value
        });
    }

    handleSearch = () => {
        let valueOfSearch = this.state.searchValue;
        if (valueOfSearch !== "") {
            alert("Doing search, but message only hehe")
        } else {
            alert("Please input any value to search.")
        }
    }

    handleRemove = (articleId) => {
        axios.delete(`http://110.74.194.124:15011/v1/api/articles/${articleId}`).then((res) => {
            alert(res.data.MESSAGE)
        })
    }

    componentDidUpdate() {
        axios
            .get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
            .then((res) => {
                this.setState({
                    article: res.data.DATA,
                });
            });
    }

    render() {
        return (
            <div>
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                    <div className="container">
                        <Navbar.Brand>AMS</Navbar.Brand>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="mr-auto">
                                <Nav.Link as={Link} to="/">Home</Nav.Link>
                            </Nav>
                            <Nav>
                                <Form inline>
                                    <Form.Group>
                                        <FormControl
                                            type="text"
                                            placeholder="Search"
                                            className="mr-sm-2"
                                            value={this.state.value}
                                            onChange={this.handleChange}
                                        />
                                    </Form.Group>
                                    <Button onClick={this.handleSearch} variant="outline-success">Search</Button>
                                </Form>
                            </Nav>
                        </Navbar.Collapse>
                    </div>
                </Navbar>
                <div className="container">
                    <div className="text-center">
                        <h1 style={{ marginTop: "15px" }}>Article Management</h1>
                        <Link to="/add">
                            <Button variant="dark" style={{ marginTop: "15px", marginBottom: "25px" }}>
                                Add New Article
                        </Button>
                        </Link>
                    </div>
                    <div>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>TITLE</th>
                                    <th>DESCRIPTION</th>
                                    <th>CREATE DATE</th>
                                    <th>IMAGE</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.article.map((data) => (
                                    <tr key={data.ID}>
                                        <td>{data.ID}</td>
                                        <td>{data.TITLE}</td>
                                        <td>{data.DESCRIPTION}</td>
                                        <td>{data.CREATED_DATE}</td>
                                        <td>
                                            <img src={data.IMAGE} style={{ width: "100px" }} alt="Avatar" />
                                        </td>
                                        <td style={{ width: "22%" }}>
                                            <Link to={`/view/${data.ID}`}>
                                                <Button variant="primary">View</Button>
                                            </Link> &nbsp;
                                        <Link to={`/update/${data.ID}`}>
                                                <Button variant="warning">Edit</Button>
                                            </Link> &nbsp;
                                        <Button
                                                variant="danger"
                                                onClick={() => this.handleRemove(data.ID)}
                                            >
                                                Delete
                                        </Button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>
        );
    }
}