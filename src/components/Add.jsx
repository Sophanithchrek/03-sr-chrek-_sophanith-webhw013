import React, { Component } from 'react'
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import { Link } from "react-router-dom"
import axios from 'axios'
import { validateFields } from './Validation'

export default class Add extends Component {

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      image: 'https://cdn.shortpixel.ai/spai/w_924+q_lossy+ret_img+to_webp/https://centralcoastdentalimplants.com/wp-content/uploads/2016/10/orionthemes-placeholder-image-750x750.jpg',
      labelTitleMsg: {
        display: "none",
        color: "red"
      },
      labelDescMsg: {
        display: "none",
        color: "red"
      },
    };
  }

  handleChange = evt => {
    this.setState({
      [evt.target.name]: evt.target.value,
    });
  }

  handleOnImageChange = evt => {
    if (evt.target.files && evt.target.files[0]) {
      this.setState({
        image: URL.createObjectURL(evt.target.files[0]),
      });
    }
  }

  handleSubmit = evt => {
    evt.preventDefault();

    const isTitleNotEmpty = validateFields.validateText(this.state.title);
    const isDescNotEmpty = validateFields.validateText(this.state.description);

    if (isTitleNotEmpty == false) {
      this.setState({
        labelTitleMsg: {
          display: "block",
          color: "red"
        },
      })
    } else {
      this.setState({
        labelTitleMsg: {
          display: "none",
          color: "red"
        },
      })
    }

    if (isDescNotEmpty == false) {
      this.setState({
        labelDescMsg: {
          display: "block",
          color: "red"
        },
      })
    } else {
      this.setState({
        labelDescMsg: {
          display: "none",
          color: "red"
        },
      })
    }

    if (isTitleNotEmpty == true && isDescNotEmpty == true) {
      let article = {
        TITLE: this.state.title,
        DESCRIPTION: this.state.description,
        IMAGE: this.state.image
      }
      axios.post("http://110.74.194.124:15011/v1/api/articles", article).then((res) => {
        alert(res.data.MESSAGE);
        this.props.history.push("/")
      })
      this.setState({
        labelTitleMsg: {
          display: "none",
          color: "red"
        },
        labelDescMsg: {
          display: "none",
          color: "red"
        },
      })
    }
  }

  render() {
    return (
      <div>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          <div className="container">
            <Navbar.Brand>AMS</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link as={Link} to="/">Home</Nav.Link>
              </Nav>
              <Nav>
                <Form inline>
                  <FormControl
                    type="text"
                    placeholder="Search"
                    className="mr-sm-2"
                  />
                  <Button variant="outline-success">Search</Button>
                </Form>
              </Nav>
            </Navbar.Collapse>
          </div>
        </Navbar>
        <div className="container">
          <h1>Add Article</h1>
          <div className="row">
            <div className="col-9">
              <Form onSubmit={this.handleSubmit} autoComplete="off">
                <Form.Group>
                  <Form.Label>Title</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter Title"
                    name="title"
                    value={this.state.title}
                    onChange={this.handleChange}
                  />
                  <p style={this.state.labelTitleMsg}>*Title Required</p>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter Description"
                    name="description"
                    value={this.state.description}
                    onChange={this.handleChange}
                  />
                  <p style={this.state.labelDescMsg}>*Descriptoin Required</p>
                </Form.Group>
                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </Form>
            </div>
            <div className="col-3">
              <img className="img-fluid" style={{ width: "100%", marginBottom: "5px" }} src={this.state.image} alt="photo" />
              <Form.File.Input onChange={this.handleOnImageChange} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
