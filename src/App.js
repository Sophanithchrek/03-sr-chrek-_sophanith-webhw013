import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import MainPage from "./components/MainPage";
import Add from "./components/Add";
import Update from "./components/Update";
import View from "./components/View";
import axios from 'axios';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      article: [],
    };
  }

  componentWillMount() {
    axios
      .get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
      .then((res) => {
        this.setState({
          article: res.data.DATA,
        });
      });
  }

  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route path="/" exact component={MainPage} />
            <Route path="/add" component={Add} />
            <Route path="/update/:ID" render={ ( props ) => <Update { ...props } dataOfArticle={ this.state.article } /> } />      
            <Route path="/view/:ID" render={ ( props ) => <View { ...props } dataOfArticle={ this.state.article } /> } />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}
